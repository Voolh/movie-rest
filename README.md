## Run of tests
```
php artisan tests
```

## Api auto docs
Launch the built-in server
```
php artisan serve
```

Wait for start of the server
```
Laravel development server started: http://127.0.0.1:8000
```

Go to browser and fill url like this
```
http://127.0.0.1:8000/docs/
``` 

