---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_6a70fbba5572fbdc9b7e1c3f6b56b3a9 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/movies?sort_title%3Dasc=ut&sort_title%3Ddesc=ipsum&sort_release_date%3Dasc=adipisci&sort_release_date%3Ddesc=nesciunt&has_genre%3Dcomedy=odit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/movies"
);

let params = {
    "sort_title=asc": "ut",
    "sort_title=desc": "ipsum",
    "sort_release_date=asc": "adipisci",
    "sort_release_date=desc": "nesciunt",
    "has_genre=comedy": "odit",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "title": "Recusandae enim facilis exercitationem.",
            "description": "Non laboriosam laboriosam veniam.",
            "release_date": "12-30-1971",
            "images": [
                {
                    "id": 1,
                    "image_url": "https:\/\/lorempixel.com\/640\/480\/?40333"
                }
            ],
            "genres": [
                {
                    "id": 11,
                    "title": "itaque"
                },
                {
                    "id": 12,
                    "title": "quia"
                },
                {
                    "id": 21,
                    "title": "odit"
                },
                {
                    "id": 23,
                    "title": "error"
                }
            ]
        },
        {
            "id": 2,
            "title": "Qui delectus nesciunt.",
            "description": "A aperiam fuga nihil.",
            "release_date": "05-23-1989",
            "images": [
                {
                    "id": 2,
                    "image_url": "https:\/\/lorempixel.com\/640\/480\/?31321"
                }
            ],
            "genres": [
                {
                    "id": 12,
                    "title": "quia"
                },
                {
                    "id": 14,
                    "title": "illo"
                },
                {
                    "id": 23,
                    "title": "error"
                }
            ]
        },
        {
            "id": 3,
            "title": "Quisquam hic dolorem eveniet.",
            "description": "Blanditiis sed qui eos qui mollitia.",
            "release_date": "02-10-1977",
            "images": [
                {
                    "id": 3,
                    "image_url": "https:\/\/lorempixel.com\/640\/480\/?16297"
                }
            ],
            "genres": [
                {
                    "id": 12,
                    "title": "quia"
                },
                {
                    "id": 14,
                    "title": "illo"
                },
                {
                    "id": 19,
                    "title": "accusantium"
                },
                {
                    "id": 24,
                    "title": "commodi"
                },
                {
                    "id": 27,
                    "title": "voluptatibus"
                }
            ]
        },
        {
            "id": 4,
            "title": "Praesentium nulla possimus quis.",
            "description": "Vel ea dicta esse et.",
            "release_date": "05-02-2001",
            "images": [
                {
                    "id": 4,
                    "image_url": "https:\/\/lorempixel.com\/640\/480\/?31235"
                }
            ],
            "genres": [
                {
                    "id": 20,
                    "title": "consequatur"
                },
                {
                    "id": 23,
                    "title": "error"
                },
                {
                    "id": 25,
                    "title": "eligendi"
                },
                {
                    "id": 30,
                    "title": "sit"
                }
            ]
        },
        {
            "id": 5,
            "title": "Nulla atque molestias.",
            "description": "Dolores optio et quo consectetur.",
            "release_date": "02-27-2010",
            "images": [
                {
                    "id": 5,
                    "image_url": "https:\/\/lorempixel.com\/640\/480\/?13602"
                }
            ],
            "genres": [
                {
                    "id": 12,
                    "title": "quia"
                },
                {
                    "id": 17,
                    "title": "velit"
                },
                {
                    "id": 23,
                    "title": "error"
                },
                {
                    "id": 28,
                    "title": "consequuntur"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/v1\/movies?page=1",
        "last": "http:\/\/localhost\/api\/v1\/movies?page=2",
        "prev": null,
        "next": "http:\/\/localhost\/api\/v1\/movies?page=2",
        "self": "http:\/\/localhost\/api\/v1\/movies"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 2,
        "path": "http:\/\/localhost\/api\/v1\/movies",
        "per_page": "5",
        "to": 5,
        "total": 10
    }
}
```

### HTTP Request
`GET api/v1/movies`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort_title=asc` |  optional  | Sort movies by title to ASC
    `sort_title=desc` |  optional  | Sort movies by title to DESC
    `sort_release_date=asc` |  optional  | Sort movies by release date to ASC
    `sort_release_date=desc` |  optional  | Sort movies by release date to DESC
    `has_genre=comedy` |  optional  | filtering movies by genre

<!-- END_6a70fbba5572fbdc9b7e1c3f6b56b3a9 -->

<!-- START_495ea5ebc4880abdaeb27ac9eac6fc57 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/movies" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"movie":{"title":"qui","description":"animi","release_date":"inventore"},"images":[{"image_url":"http:\/\/example.tld\/image.png"}],"genres":[{"title":"sed"}]}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/movies"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "movie": {
        "title": "qui",
        "description": "animi",
        "release_date": "inventore"
    },
    "images": [
        {
            "image_url": "http:\/\/example.tld\/image.png"
        }
    ],
    "genres": [
        {
            "title": "sed"
        }
    ]
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/movies`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `movie` | array |  required  | 
        `movie.title` | string |  required  | The title of the movie
        `movie.description` | string |  required  | The description of the movie
        `movie.release_date` | string |  required  | The release day of the movie
        `images` | array |  required  | 
        `images.*.image_url` | string |  optional  | Url of the movie image.
        `genres` | array |  required  | 
        `genres.*.title` | string |  required  | Title of movie genre
    
<!-- END_495ea5ebc4880abdaeb27ac9eac6fc57 -->

<!-- START_5d8fdf5650535985049f9ce21cb91435 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/movies/odio" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/movies/odio"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 10,
        "title": "Qui error.",
        "description": "Sed eligendi eveniet explicabo eius maxime.",
        "release_date": "06-14-2011"
    },
    "images": [
        {
            "id": 10,
            "image_url": "https:\/\/lorempixel.com\/640\/480\/?61398"
        }
    ],
    "genres": [
        {
            "id": 28,
            "title": "labore"
        },
        {
            "id": 11,
            "title": "sequi"
        }
    ]
}
```
> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Movie]"
}
```

### HTTP Request
`GET api/v1/movies/{movie}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `movie` |  required  | The integer ID of the movie.

<!-- END_5d8fdf5650535985049f9ce21cb91435 -->

<!-- START_ea1b1be2875b766626120078364473d7 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/movies/rem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":12,"title":"perferendis","description":"aut","release_date":"quis","images":[{"image_url":"http:\/\/example.tld\/image.png"}],"genres":[{"title":"nemo"}]}'

```

```javascript
const url = new URL(
    "http://localhost/api/v1/movies/rem"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 12,
    "title": "perferendis",
    "description": "aut",
    "release_date": "quis",
    "images": [
        {
            "image_url": "http:\/\/example.tld\/image.png"
        }
    ],
    "genres": [
        {
            "title": "nemo"
        }
    ]
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/movies/{movie}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `movie` |  required  | The integer ID of the movie.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | The existing movie ID
        `title` | string |  required  | The title of the movie
        `description` | string |  required  | The description of the movie
        `release_date` | string |  required  | The release day of the movie
        `images` | array |  required  | 
        `images.*.image_url` | string |  optional  | Url of the movie image.
        `genres` | array |  required  | 
        `genres.*.title` | string |  required  | Title of movie genre
    
<!-- END_ea1b1be2875b766626120078364473d7 -->

<!-- START_f6744022488f66a7bbb84ce1b1e105f2 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/movies/id" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/movies/id"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/movies/{movie}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `movie` |  required  | The integer ID of the movie.

<!-- END_f6744022488f66a7bbb84ce1b1e105f2 -->


