<?php

namespace Tests\Unit\Models;

use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieImages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class MovieTest extends TestCase
{
    private $model;

    protected function setUp(): void
    {
        parent::setUp();
        $this->model = factory(Movie::class)->make();
    }

    /**
     * @test
     * @group models
     */
    public function movieModelExists()
    {
        $this->assertInstanceOf(Model::class, $this->model);
    }

    /**
     * @test
     * @group models
     */
    public function movieModelHasExpectedFillableFields()
    {
        $expectedFields = ['title', 'description', 'release_date'];
        $actual = $this->model->getFillable();

        asort($expectedFields);
        asort($actual);

        $this->assertSame($expectedFields, $actual);
    }

    /**
     * @test
     * @group models
     */
    public function movieModelHasGuardedId()
    {
        $guarded = $this->model->getGuarded();

        $this->assertNotEmpty($guarded);
        $this->assertContains('id', $guarded);
    }

    /**
     * @test
     * @group models
     */
    public function movieModelHasImagesRelation()
    {
        $foreignKey = 'movie_id';
        $relationship = $this->model->images();
        $relatedModel = $relationship->getRelated();

        $this->assertInstanceOf(HasMany::class, $relationship);
        $this->assertInstanceOf(MovieImages::class, $relatedModel);
        $this->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }

    /**
     * @test
     * @group models
     */
    public function movieModelHasGenresRelation()
    {
        $foreignKey = 'movie_id';
        $relationship = $this->model->genres();
        $relatedModel = $relationship->getRelated();

        $this->assertInstanceOf(BelongsToMany::class, $relationship);
        $this->assertInstanceOf(Genre::class, $relatedModel);
        $this->assertEquals($foreignKey, $relationship->getForeignPivotKeyName());
    }
}
