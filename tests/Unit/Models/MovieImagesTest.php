<?php

namespace Tests\Unit\Models;

use App\Models\Movie;
use App\Models\MovieImages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Tests\TestCase;

class MovieImagesTest extends TestCase
{
    private $model;

    protected function setUp(): void
    {
        parent::setUp();
        $this->model = factory(MovieImages::class)->states(['has_id', 'has_movie_id'])->make();
    }

    /**
     * @test
     * @group models
     */
    public function movieImagesModelExists()
    {
        $this->assertInstanceOf(Model::class, $this->model);
    }

    /**
     * @test
     * @group models
     */
    public function movieImagesModelHasExpectedFillableFields()
    {
        $expectedFields = ['movie_id', 'image_url'];
        $actual = $this->model->getFillable();

        asort($expectedFields);
        asort($actual);

        $this->assertSame($expectedFields, $actual);
    }

    /**
     * @test
     * @group models
     */
    public function movieImagesModelHasGuardedId()
    {
        $guarded = $this->model->getGuarded();

        $this->assertNotEmpty($guarded);
        $this->assertContains('id', $guarded);
    }

    /**
     * @test
     * @group models
     */
    public function movieImagesModelHasMovieRelation()
    {
        $foreignKey = 'movie_id';
        $relationship = $this->model->movie();
        $relatedModel = $relationship->getRelated();

        $this->assertInstanceOf(BelongsTo::class, $relationship);
        $this->assertInstanceOf(Movie::class, $relatedModel);
        $this->assertEquals($foreignKey, $relationship->getForeignKeyName());
    }
}
