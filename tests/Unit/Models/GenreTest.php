<?php

namespace Tests\Unit\Models;

use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tests\TestCase;

class GenreTest extends TestCase
{
    private $model;

    protected function setUp(): void
    {
        parent::setUp();
        $this->model = factory(Genre::class)->make();
    }

    /**
     * @test
     * @group models
     */
    public function genreModelExists()
    {
        $this->assertInstanceOf(Model::class, $this->model);
    }

    /**
     * @test
     * @group models
     */
    public function genreModelHasExpectedFillableFields()
    {
        $expectedFields = ['title'];
        $actual = $this->model->getFillable();

        asort($expectedFields);
        asort($actual);

        $this->assertSame($expectedFields, $actual);
    }

    /**
     * @test
     * @group models
     */
    public function genreModelHasMoviesRelation()
    {
        $foreignKey = 'genre_id';
        $relationship = $this->model->movies();
        $relatedModel = $relationship->getRelated();

        $this->assertInstanceOf(BelongsToMany::class, $relationship);
        $this->assertInstanceOf(Movie::class, $relatedModel);
        $this->assertEquals($foreignKey, $relationship->getForeignPivotKeyName());
    }
}
