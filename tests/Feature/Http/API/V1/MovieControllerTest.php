<?php

namespace Tests\Feature\Http\API\V1;

use App\Http\Resources\MovieResource;
use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieImages;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MovieControllerTest extends TestCase
{
    use RefreshDatabase;

    private $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Factory::create();
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanUpdateMovieById()
    {
        $movieId = random_int(1,10);
        $movie = Movie::with(['images', 'genres'])->find($movieId);
        $jsonMovie = (new MovieResource($movie))->toJson();
        $arrayMovie = json_decode($jsonMovie, true);
        $arrayMovie['title'] = $newMovieTitle = $this->faker->sentence(2);
        $arrayMovie['images'][] = $newImage = \factory(MovieImages::class)->make()->toArray();
        $arrayMovie['genres'][] = $newGenre = \factory(Genre::class)->make()->toArray();

        $resourcePath = 'api/v1/movies/'.$movieId;
        $response = $this->put($resourcePath, $arrayMovie);
        $response->assertRedirect($resourcePath);
        $this->assertDatabaseHas('movies', ['title' => $newMovieTitle]);
        $this->assertDatabaseHas('movie_images', ['image_url' => $newImage['image_url']]);
        $this->assertDatabaseHas('genres', ['title' => $newGenre['title']]);
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanReceiveListOfMovies()
    {
        $response = $this->get('api/v1/movies');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanNavigateTroughPaginationUrlsInListOfMovies()
    {
        $response = $this->get('api/v1/movies');
        $response->assertStatus(200);

        $json = $response->json();
        $next = $json['links']['next'];
        $currentPage = $json['meta']['current_page'];

        $parts = parse_url($next);
        $link = $parts['path'].'/?'.$parts['query'];

        $nextResponse = $this->get($link);
        $nextResponse->assertStatus(200);
        $nextJson = $nextResponse->json();
        $nextCurrentPage = $nextJson['meta']['current_page'];
        $this->assertGreaterThan($currentPage, $nextCurrentPage);
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanSortListOfMoviesByTitle()
    {
        $sortParam = 'sort_title';
        $response = $this->get('api/v1/movies?' . $sortParam . '=desc');

        $resultItems = $response->json()['data'];

        //Get only titles from results
        $resultTitles = array_map(
            function ($item) { return $item['title']; },
            $resultItems
        );

        //Copy titles
        $copyResultTitlesForSort = $resultTitles;

        //Sorting
        sort($copyResultTitlesForSort, SORT_NATURAL | SORT_FLAG_CASE);

        $this->assertSame($resultTitles, $copyResultTitlesForSort);

        $response->assertStatus(200);
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanReceiveConcreteMovieByGenre()
    {
        $response = $this->get('api/v1/movies');
        $existedGenre = $response->json()['data'][0]['genres'][0]['title'];
        $response = $this->get('api/v1/movies?has_genre=' . $existedGenre);

        $resultItems = $response->json()['data'];

        $foundedGenres = 0;

        //Compare movie items in results & check that every item has expected genre
        foreach ($resultItems as $movie) {
            foreach ($movie['genres'] as $genre) {
                if ($genre['title'] === $existedGenre) {
                    ++$foundedGenres;
                }
            }
        }
        $response->assertStatus(200);
        $this->assertEquals(count($resultItems), $foundedGenres);
    }

    /**
     * @test
     * @group controllers
     */
    public function requestWillReturnsEmptyResultWhenClientSetsEmptyGenre()
    {
        $response = $this->get('api/v1/movies?has_genre=');
        $response->assertStatus(200);
        $this->assertEmpty($response->json()['data']);
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanDeleteMovieById()
    {
        $movieId = random_int(1,10);
        $response = $this->delete('api/v1/movies/' . $movieId);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanStoreNewMovieWithRelations()
    {
        $movie = factory(Movie::class)->make()->toArray();
        $images = factory(MovieImages::class, 2)->make()->toArray();
        $genres = factory(Genre::class, 3)->make()->toArray();

        $storeData = compact('movie', 'images', 'genres');
        $response = $this->post('api/v1/movies/', $storeData);

        $response->assertRedirect();
    }

    /**
     * @test
     * @group controllers
     */
    public function clientCanReceiveMovieById()
    {
        $movieId = random_int(1,10);
        $response = $this->get('api/v1/movies/' . $movieId);
        $response->assertStatus(200);
    }
}
