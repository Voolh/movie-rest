<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API\V1')
    ->prefix('v1')
    ->group(function () {
        Route::get('movies', 'MovieController@index')->name('movies.index');
        Route::post('movies', 'MovieController@store')->name('movies.store');
        Route::get('movies/{movie}', 'MovieController@show')->name('movies.show');
        Route::put('movies/{movie}', 'MovieController@update')->name('movies.update');
        Route::delete('movies/{movie}', 'MovieController@destroy')->name('movies.delete');
    });
