<?php


namespace App\Filters;

use Illuminate\Http\Request;

class MovieFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function sort_title($type = null) {
        return $this->builder->orderBy('title', (!$type || $type === 'asc') ? 'desc' : 'asc');
    }

    public function sort_release_date($type = null) {
        return $this->builder->orderBy('release_date', (!$type || $type === 'asc') ? 'desc' : 'asc');
    }

    public function has_genre($type = null) {
        return $this->builder->whereHas('genres', function ($query) use ($type) {
            return $query->where('title', '=', $type);
        });
    }
}
