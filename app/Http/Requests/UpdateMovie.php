<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMovie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:movies',
            'title' => 'required|string|max:256',
            'description' => 'required|string|max:256',
            'release_date' => 'required|string',
            'images' => 'required|array|min:1',
            'images.*.image_url' => 'required|url',
            'genres' => 'required|array|min:1',
            'genres.*.title' => 'required|string',
        ];
    }
}
