<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMovie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'movie' => 'required|array|min:1',
            'movie.title' => 'required|string|max:256',
            'movie.description' => 'required|string|max:1024',
            'movie.release_date' => 'required|string',
            'images' => 'required|array|min:1',
            'images.*.image_url' => 'required|url',
            'genres' => 'required|array|min:1',
            'genres.*.title' => 'required|string',
        ];
    }
}
