<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->id,
            'title' => (string) $this->title,
            'description' => (string) $this->description,
            'release_date' => (string) $this->release_date,
            'images' => MovieImagesResource::collection($this->whenLoaded('images')),
            'genres' => GenreResource::collection($this->whenLoaded('genres')),
        ];
    }
}
