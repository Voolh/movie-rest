<?php

namespace App\Http\Controllers\API\V1;

use App\Filters\MovieFilters;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMovie;
use App\Http\Requests\UpdateMovie;
use App\Http\Resources\MovieCollection;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Services\MovieRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MovieController extends Controller
{
    private $movie;
    private $movieRepository;
    private $perPage;

    public function __construct()
    {
        $this->movie = app(Movie::class);
        $this->movieRepository = app(MovieRepository::class);
        $this->perPage = config('api.per_page');
    }

    /**
     * Display a listing of the resource.
     *
     * @queryParam  sort_title=asc Sort movies by title to ASC
     * @queryParam  sort_title=desc Sort movies by title to DESC
     * @queryParam  sort_release_date=asc Sort movies by release date to ASC
     * @queryParam  sort_release_date=desc Sort movies by release date to DESC
     * @queryParam  has_genre=comedy filtering movies by genre
     *
     * @param Request $request
     * @param MovieFilters $filters
     * @return MovieCollection
     */
    public function index(Request $request, MovieFilters $filters)
    {
        $filter = $this->movie::filter($filters);

        $movies = $filter->with(['images', 'genres'])->paginate($this->perPage);

        return new MovieCollection($movies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @bodyParam  movie array required
     * @bodyParam  movie.title string required The title of the movie
     * @bodyParam  movie.description string required The description of the movie
     * @bodyParam  movie.release_date string required The release day of the movie
     * @bodyParam  images array required
     * @bodyParam  images.*.image_url string Url of the movie image. Example: http://example.tld/image.png
     * @bodyParam  genres array required
     * @bodyParam  genres.*.title string required Title of movie genre
     *
     * @param StoreMovie $request
     * @return RedirectResponse
     */
    public function store(StoreMovie $request)
    {
        $movieId = $this->movieRepository->store($request);

        return redirect()->route('movies.show', ['movie' => $movieId]);
    }

    /**
     * Display the specified resource.
     *
     * @urlParam  movie required The integer ID of the movie.
     * @response  200 {
     *  "data": {"id":10, "title":"Qui error.", "description":"Sed eligendi eveniet explicabo eius maxime.", "release_date":"06-14-2011"},
     *  "images": [{"id":10,"image_url":"https:\/\/lorempixel.com\/640\/480\/?61398"}],
     *  "genres": [{"id":28,"title":"labore"},{"id":11,"title":"sequi"}]
     * }
     *
     * @response  404 {
     *  "message": "No query results for model [App\\Models\\Movie]"
     * }
     *
     * @param  int  $id
     * @return MovieResource
     */
    public function show($id)
    {
        $movie = $this->movie->with(['images', 'genres'])->findOrFail($id);
        return new MovieResource($movie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @urlParam  movie required The integer ID of the movie.
     * @bodyParam  id integer required The existing movie ID
     * @bodyParam  title string required The title of the movie
     * @bodyParam  description string required The description of the movie
     * @bodyParam  release_date string required The release day of the movie
     * @bodyParam  images array required
     * @bodyParam  images.*.image_url string Url of the movie image. Example: http://example.tld/image.png
     * @bodyParam  genres array required
     * @bodyParam  genres.*.title string required Title of movie genre
     *
     * @param UpdateMovie $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateMovie $request, $id)
    {
        $this->movieRepository->update($request, $id);

        return redirect()->route('movies.show', ['movie' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @urlParam  movie required The integer ID of the movie.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $movie = $this->movie->findOrFail($id);
        $movie->delete();
        return response('Entity successfully deleted', 200);
    }
}
