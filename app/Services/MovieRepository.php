<?php


namespace App\Services;


use App\Http\Requests\StoreMovie;
use App\Http\Requests\UpdateMovie;
use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieImages;
use Illuminate\Support\Facades\DB;

class MovieRepository
{

    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    private $movieImages;
    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    private $genres;
    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    private $movie;
    /**
     * @var array
     */
    private $validatedData;

    public function __construct()
    {
        $this->genres = app(Genre::class);
        $this->movieImages = app(MovieImages::class);
        $this->movie = app(Movie::class);
    }

    /**
     * @param StoreMovie $request
     * @return int
     */
    public function store(StoreMovie $request): int
    {
        $this->validatedData = $request->validated();

        $newMovie = $this->movie->create($this->validatedData['movie']);

        $this->storeImages($newMovie->id);

        $this->storeGenres($newMovie->id);

        return $newMovie->id;
    }

    /**
     * @param UpdateMovie $request
     * @param $movieId
     * @return int
     */
    public function update(UpdateMovie $request, $movieId): int
    {
        $this->validatedData = $request->validated();

        $movie = $this->movie->where('id', $movieId)
            ->update(
                [
                    'title' => $this->validatedData['title'],
                    'description' => $this->validatedData['description'],
                    'release_date' => $this->validatedData['release_date']
                ]
            );

        $this->updateImages($movieId);
        $this->updateGenres($movieId);

        return $movieId;
    }

    /**
     * @param int $movieId
     */
    private function updateImages(int $movieId): void
    {
        $this->deleteImages($movieId);
        $this->storeImages($movieId);
    }

    /**
     * @param int $movieId
     */
    private function deleteImages(int $movieId): void
    {
        $this->movieImages->where('movie_id', $movieId)->delete();
    }

    /**
     * @param int $movieId
     */
    private function updateGenres(int $movieId): void
    {
        $this->deleteGenres($movieId);
        $this->storeGenres($movieId);
    }

    /**
     * @param int $movieId
     */
    private function deleteGenres(int $movieId): void
    {
        DB::table('genre_movie')->where('movie_id', $movieId)->delete();
    }

    /**
     * @param int $movieId
     */
    private function storeImages(int $movieId): void
    {
        foreach ($this->validatedData['images'] as $image) {
            $this->movieImages->create([
                'image_url' => $image['image_url'],
                'movie_id' => $movieId
            ]);
        }
    }

    /**
     * @param int $movieId
     */
    private function storeGenres(int $movieId): void
    {
        foreach ($this->validatedData['genres'] as $genre) {
            $genre = $this->genres->firstOrCreate(['title' => $genre['title']]);
            DB::table('genre_movie')
                ->insert(['movie_id' => $movieId, 'genre_id' => $genre->id]);
        }
    }
}
