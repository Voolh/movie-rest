<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MovieImages extends Model
{
    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'movie_id', 'image_url',
    ];

    /**
     * @return BelongsTo
     */
    public function movie(): BelongsTo
    {
        return $this->belongsTo(Movie::class);
    }
}
