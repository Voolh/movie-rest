<?php

namespace App\Models;

use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Movie extends Model
{

    use Filterable;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'title', 'description', 'release_date',
    ];

    public function images(): HasMany
    {
        return $this->hasMany(MovieImages::class);
    }

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class);
    }
}
