<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3, true),
        'description' => $faker->sentence(5, true),
        'release_date' => $faker->date($format = 'm-d-Y'),
    ];
});

$factory->state(Movie::class, 'has_id', function (Faker $faker) {
    return [
        'id' => $faker->randomNumber(1),
    ];
});
