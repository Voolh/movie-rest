<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Genre;
use Faker\Generator as Faker;

$factory->define(Genre::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->words(1, true),
    ];
});

$factory->state(Genre::class, 'has_id', function (Faker $faker) {
    return [
        'id' => $faker->randomNumber(1),
    ];
});

