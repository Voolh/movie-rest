<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\MovieImages;
use Faker\Generator as Faker;

$factory->define(MovieImages::class, function (Faker $faker) {
    return [
        'image_url' => $faker->imageUrl(),
    ];
});

$factory->state(MovieImages::class, 'has_id', function (Faker $faker) {
    return [
        'id' => $faker->randomNumber(1),
    ];
});

$factory->state(MovieImages::class, 'has_movie_id', function (Faker $faker) {
    return [
        'movie_id' => $faker->randomNumber(1),
    ];
});


