<?php

use App\Models\Movie;
use App\Models\MovieImages;
use Illuminate\Database\Seeder;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Movie::class, 10)
            ->create()
            ->each(function ($movie) {
                $movie->images()->save(factory(MovieImages::class)->make());
            });
    }
}
