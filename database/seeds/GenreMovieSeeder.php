<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreMovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $moviesIds = DB::table('movies')->pluck('id')->toArray();
        $genresIds = DB::table('genres')->pluck('id')->toArray();

        foreach ($moviesIds as $movieId) {
            $genreNums = random_int(1, 5);
            $randomGenreIdsKeys = array_rand($genresIds, $genreNums);

            if (!is_array($randomGenreIdsKeys)) {
                $randomGenreIdsKeys = [$randomGenreIdsKeys];
            }

            foreach ($randomGenreIdsKeys as $idKey) {
                DB::table('genre_movie')->insert([
                    'genre_id' => $genresIds[$idKey],
                    'movie_id' => $movieId
                ]);
            }
        }
    }
}
